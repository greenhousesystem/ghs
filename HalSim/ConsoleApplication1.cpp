// ConsoleApplication1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "HumiditySensorFactory.h"
#include "TemperatureFactory.h"
#include "WaterningService.h"
#include "FanService.h"
#include "Hal.h"

int main()
{
    iSensor* tempsensor = TemperatureFactory().createTemperatureSensor();
    iSensor* humiditySensor = HumiditySensorFactory().createHumiditySensor();

    Hal hal;
    hal.addSensor(tempsensor);
    hal.addSensor(humiditySensor);
    hal.hal_ghs_init();


    WaterningService obj(hal);
    FanService fsobj(hal);

    while(1)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(200));
    }
    std::cout << "Hello World!\n";
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started:
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
