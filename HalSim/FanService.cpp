#include "FanService.h"

FanService::FanService(Hal& hal): mHal(hal)
{
    mThread = std::thread([this]()
    {
        run();
    });
}

void FanService::run()
{
    while(true)
    {
        int data;
        mHal.hal_read_humidity(&data);
        std::this_thread::sleep_for(std::chrono::milliseconds(200));
    }
}
