#pragma once
#include "Hal.h"
#include <thread>

class FanService
{
private:
    Hal& mHal;
    std::thread mThread;
public:
    FanService(Hal& hal);
    void run();
};

