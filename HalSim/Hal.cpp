#include "Hal.h"

void Hal::addSensor(iSensor* sensor)
{
    mSensor[sensor->getType()].push_back(sensor);
}

int Hal::hal_ghs_init()
{
    for(auto sensor : mSensor)
    {
        for(auto sensortype : sensor.second)
        {
            sensortype->startSensor();
        }
    }
    return 0;
}

int Hal::hal_read_temperature(ghs_temp_sensor_zone_t ghs_zone, float& temp)
{
    if(mSensor.count(SensorType::TemperatureSen))
    {
        for(auto sensor : mSensor[SensorType::TemperatureSen])
        {
            if(sensor->getType() == SensorType::TemperatureSen)
            {
                void* ptr = nullptr;
                sensor->readSensorData(ptr);
            }
        }
    }
    return 0;
}

int Hal::hal_read_humidity(int* humiVal)
{
    if(mSensor.count(SensorType::Humidity))
    {
        for(auto sensor : mSensor[SensorType::Humidity])
        {
            if(sensor->getType() == SensorType::Humidity)
            {
                void* ptr = nullptr;
                sensor->readSensorData(ptr);
            }
        }
    }
    return 0;
}
