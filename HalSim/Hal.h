#pragma once

#include "TemperatureFactory.h"
#include "HumiditySensorFactory.h"
#include <vector>
#include <unordered_map>

typedef enum
{
    GHS_TEMP_SENSOR_ZONE1 = 1,
    GHS_TEMP_SENSOR_ZONE2,
    GHS_TEMP_SENSOR_ZONE3,
    GHS_TEMP_SENSOR_ZONE4
} ghs_temp_sensor_zone_t;

class Hal
{
private:
    std::unordered_map <SensorType, std::vector<iSensor*>> mSensor;
public:
    void addSensor(iSensor* sensor);
    int hal_ghs_init();
    int hal_read_temperature(ghs_temp_sensor_zone_t ghs_zone, float& temp);
    int hal_read_humidity(int* humiVal);
};

