#include "HumiditySensor.h"
#include <iostream>

using namespace std;

HumiditySensor::HumiditySensor(): mSensorType(SensorType::Humidity)
{
}

bool HumiditySensor::startSensor()
{
    cout << "starting Humidity sensor" << std::endl;

    return false;
}

void HumiditySensor::readSensorData(void* ptr)
{
    cout << "Reading Humidity Sensor" << std::endl;
}

SensorType HumiditySensor::getType()
{
    return mSensorType;
}
