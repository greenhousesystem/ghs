#pragma once

#include "iSensor.h"

class HumiditySensor :
    public iSensor
{
    SensorType mSensorType;
public:
    HumiditySensor();
    bool startSensor() override;
    void readSensorData(void* ptr) override;
    SensorType getType() override;
};

