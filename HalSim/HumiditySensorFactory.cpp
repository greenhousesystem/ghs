#include "HumiditySensorFactory.h"
#include "HumiditySensor.h"

iSensor* HumiditySensorFactory::createHumiditySensor()
{
    return new HumiditySensor();
}
