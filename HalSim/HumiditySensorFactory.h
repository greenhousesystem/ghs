#pragma once
#include "iSensor.h"

class HumiditySensorFactory
{
public:

    iSensor* createHumiditySensor();
};

