#include "Temperature.hpp"

#include <iostream>


using namespace std;

Temperature::Temperature(): mSensorType(SensorType::TemperatureSen)
{
}

bool Temperature::startSensor()
{
    cout << "starting Temperature sensor" << std::endl;

    return false;
}

void Temperature::readSensorData(void* ptr)
{
    cout << "Reading Temperature Sensor" << std::endl;
}

SensorType Temperature::getType()
{
    return mSensorType;
}
