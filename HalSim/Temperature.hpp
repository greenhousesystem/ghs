#pragma once

#include "iSensor.h"

class Temperature: public iSensor
{
    SensorType mSensorType;
public:
    Temperature();
    bool startSensor() override;
    void readSensorData(void* ptr) override;
    SensorType getType() override;
};

