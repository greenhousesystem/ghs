#include "TemperatureFactory.h"
#include "Temperature.hpp"

iSensor* TemperatureFactory::createTemperatureSensor()
{
    return new Temperature();
}
