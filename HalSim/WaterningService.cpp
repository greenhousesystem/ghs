#include "WaterningService.h"

WaterningService::WaterningService(Hal& hal): mHal(hal)
{
    mThread = std::thread([this]()
    {
        checkHumidty();
    });
}

void WaterningService::checkHumidty()
{
    while(true)
    {
        float data;
        mHal.hal_read_temperature(GHS_TEMP_SENSOR_ZONE1, data);
        std::this_thread::sleep_for(std::chrono::milliseconds(200));
    }
}
