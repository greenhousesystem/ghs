#pragma once
#include "Hal.h"
#include <thread>

class WaterningService
{
private:
    Hal& mHal;
    std::thread mThread;
public:
    WaterningService(Hal& hal);
    void checkHumidty();
};

