#pragma once
enum SensorType
{
    Humidity = 1,
    TemperatureSen,
};

class iSensor
{
public:
    virtual ~iSensor() = default;

    virtual bool startSensor() = 0;

    virtual void readSensorData(void* ptr) = 0;

    virtual SensorType getType() = 0;
};

