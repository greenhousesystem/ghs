#include "Hal.h"
#include "HardwareSerial.h"

Hal::Hal(ILogger& log):mLog(log)
{

}

void Hal::addSensor(iSensor* sensor)
{
    mSensor[sensor->getType()].push_back(sensor);
}

int Hal::hal_ghs_init()
{
    for(auto sensor : mSensor)
    {
        for(auto sensortype : sensor.second)
        {
            if(!sensortype->startSensor())
            {
               mLog << sensortype->getName() << " Sensor failed to start" <<ILogger::end;
            }
        }
    }
    return 0;
}

int Hal::hal_read_temperature(ghs_temp_sensor_zone_t ghs_zone, float& temp)
{
    if(mSensor.count(SensorType::TemperatureSen))
    {
        for(auto sensor : mSensor[SensorType::TemperatureSen])
        {
            if(sensor->getType() == SensorType::TemperatureSen)
            {
                sensor->readSensorData(&temp);
            }
        }
    }
    return 0;
}

int Hal::hal_read_Depth(int* depth)
{
    if(mSensor.count(SensorType::DistanceSen))
    {
        for(auto sensor : mSensor[SensorType::DistanceSen])
        {
            if(sensor->getType() == SensorType::DistanceSen)
            {
                sensor->readSensorData(depth);
            }
        }
    }
    return 0;
}
