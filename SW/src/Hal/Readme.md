Hal --Hardware abstraction layer
===================================

### Background
This layer kind of acts as library abstraction layer even though name is HAL
basic idea is to abstract all the libraries being used, hal will act as single point of contact for application or middleware to communicate with the platform.

Don't include any application header file here 
