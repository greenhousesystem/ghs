#include "Distance.hpp"
#include <Wire.h>
#include "VL53L0X.h"
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include "FunctionalInterrupt.h"

using namespace std;

Distance::Distance() :setname(), mSensorType(SensorType::DistanceSen),mSensor(new VL53L0X())
{
    mSemaphore = xSemaphoreCreateBinary();
    pinMode(18, INPUT);
    Wire.begin(21, 22, 400000);
    attachInterrupt(
        18, [this]() { intHandler(); }, FALLING);
       // sensorInit(1000);
}

void Distance::intHandler()
{
    static BaseType_t xHigherPriorityTaskWoken;
    xHigherPriorityTaskWoken = false;
    xSemaphoreGiveFromISR(mSemaphore, &xHigherPriorityTaskWoken);
    if (xHigherPriorityTaskWoken)
    {
        portYIELD_FROM_ISR();
    }
}

bool Distance::startSensor()
{
   bool init=sensorInit(1000);
   xTaskCreateUniversal(&Distance::run, "arduino_task", 8192, (void *)this, 5, NULL, 1);
   return init;
}

void Distance::readSensorData(void *ptr)
{
  *((int*)ptr)=mDistance;
}

SensorType Distance::getType()
{
    return mSensorType;
}


void Distance::run(void *pvParameter)
{
    Distance *obj = (Distance *)pvParameter;
    while (1)
    {
        if (xSemaphoreTake(obj->mSemaphore, 2000 /*portMAX_DELAY*/))
        {
            int sensorData;
            sensorData = obj->mSensor->readRangeContinuousMillimeters(); // prit range in mm to serial monitor
            if (obj->mSensor->timeoutOccurred())
            {
                //timeout--> something happened to the sensor
                //Todo ,take care of the scenario
            }
            else
            {
                obj->mDistance=sensorData;
            }
        }
    }
}

bool Distance::sensorInit(uint32_t period)
{
    bool ret;
    ret = mSensor->init();
    mSensor->setTimeout(50);
    delay(100);
    mSensor->setMeasurementTimingBudget(200000);
    delay(100);
    mSensor->startContinuous(period);
    delay(100);
    mSensor->readRangeContinuousMillimeters();
    return ret;
}