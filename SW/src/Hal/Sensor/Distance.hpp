#pragma once

#include "iSensor.h"
#include "FreeRTOS.h"

//forward Declaration
class VL53L0X;

class Distance: public iSensor
{
    SensorType mSensorType;
    VL53L0X* mSensor;
    volatile SemaphoreHandle_t mSemaphore;
    int mDistance;
public:
    Distance();
    bool startSensor() override;
    void readSensorData(void* ptr) override;
    SensorType getType() override;

    Distance(const Distance& rhs)= delete;
    Distance& operator =(const Distance& rhs)= delete;
private:
   void intHandler();
   static void run(void * ptr);
   bool sensorInit(uint32_t period);
};

