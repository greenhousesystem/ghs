#include "DistanceSensorFactory.h"
#include "Distance.hpp"

iSensor* DistanceSensorFactory::createHumiditySensor()
{
    return new Distance();
}
