#include "Temperature.hpp"
#include "DS18B20.h"
#include "OneWire.h"
#include <iostream>

using namespace std;

Temperature::Temperature() :setname(), mSensorType(SensorType::TemperatureSen)
{
    mOneWire =new OneWire(2);
    mSensor=new DallasTemperature(mOneWire);
}

bool Temperature::startSensor()
{
    mOneWire->begin(2);
    mSensor->setPullupPin(2);
    mSensor->begin();
    auto deviceCount = mSensor->getDeviceCount();
    Serial.printf("count:%d \n",deviceCount);
    if (!mSensor->getAddress(mAddress, 0))
    {
        Serial.println("Unable to find address for Device 0");
        return false;
    }
      for (uint8_t i = 0; i < 8; i++)
  {
    if (mAddress[i] < 16) Serial.print("0");
    Serial.print(mAddress[i], HEX);
  }
  Serial.printf("\n");
    mSensor->setResolution(mAddress, 9);
    float tempC = mSensor->getTempC(mAddress);
    Serial.println(tempC);
    return false;
}

void Temperature::readSensorData(void *ptr)
{
    mSensor->requestTemperatures();
    float tempC = mSensor->getTempC(mAddress);
    memcpy(ptr, &tempC, sizeof(tempC));
    if (tempC == DEVICE_DISCONNECTED_C)
    {
        Serial.println("Error: Could not read temperature data");
        return;
    };
}

SensorType Temperature::getType()
{
    return mSensorType;
}
