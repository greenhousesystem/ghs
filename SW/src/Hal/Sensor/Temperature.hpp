#pragma once

#include "iSensor.h"
#include <OneWire.h>
#include <DallasTemperature.h>

class Temperature: public iSensor
{
    SensorType mSensorType;
    OneWire* mOneWire;
    DallasTemperature* mSensor;
    DeviceAddress mAddress;
public:
    Temperature();
    bool startSensor() override;
    void readSensorData(void* ptr) override;
    SensorType getType() override;

    Temperature(const Temperature& rhs)= delete;
    Temperature& operator =(const Temperature& rhs)= delete;
};

