#pragma once

#include "iSensor.h"

class TemperatureFactory
{

public:
    iSensor* createTemperatureSensor();
};

