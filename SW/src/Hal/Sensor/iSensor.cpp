#include "iSensor.h"
#include "esp32-hal-log.h"

iSensor::iSensor(std::string name) :mName(std::move(name))
{
  //Removing .cpp
  mName.pop_back();
  mName.pop_back();
  mName.pop_back();
  mName.pop_back();
}


std::string& iSensor::getName()
{
    return mName;
}