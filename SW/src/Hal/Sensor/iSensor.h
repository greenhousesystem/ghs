#pragma once
#include <string>

enum SensorType
{
    Humidity = 1,
    TemperatureSen,
    DistanceSen,
};

class iSensor
{
    std::string mName;
public:
    iSensor(std::string name);

    virtual ~iSensor() = default;

    virtual bool startSensor() = 0;

    virtual void readSensorData(void* ptr) = 0;

    virtual SensorType getType() = 0;

    virtual std::string& getName();
};

#define setname() iSensor(pathToFileName(__FILE__))


