#pragma once
#include <string>

class ILogger
{
public:
    static constexpr char *end = "\r\n";

public:
    virtual ILogger &operator<<(const std::string &data) = 0;

    virtual ILogger &operator<<(const char *data) = 0;

    virtual ILogger &operator<<(const int data) = 0;

    virtual ILogger &operator<<(const float data) = 0;
};