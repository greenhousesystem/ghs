#include "WifiIF.hpp"
extern "C"
{
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <string.h>
#include <esp_err.h>
#include <esp_wifi.h>
#include <esp_event_loop.h>
#include <esp32-hal.h>
#include <lwip/ip_addr.h>
#include "lwip/err.h"
#include "lwip/dns.h"
#include <esp_smartconfig.h>
#include <tcpip_adapter.h>

}

static bool sta_config_equal(const wifi_config_t &lhs, const wifi_config_t &rhs)
{
    if (memcmp(&lhs, &rhs, sizeof(wifi_config_t)) != 0)
    {
        return false;
    }
    return true;
}

bool WifiIF::_autoReconnect = true;
bool WifiIF::_useStaticIp = false;

static wl_status_t _sta_status = WL_NO_SHIELD;
static EventGroupHandle_t _sta_status_group = NULL;

/**
 * Return Connection status.
 * @return one of the value defined in wl_status_t
 *
 */
wl_status_t WifiIF::status()
{
    if (!_sta_status_group)
    {
        return _sta_status;
    }
    return (wl_status_t)xEventGroupClearBits(_sta_status_group, 0);
}

void WifiIF::_setStatus(wl_status_t status)
{
    if (!_sta_status_group)
    {
        _sta_status_group = xEventGroupCreate();
        if (!_sta_status_group)
        {
            log_e("STA Status Group Create Failed!");
            _sta_status = status;
            return;
        }
    }
    xEventGroupClearBits(_sta_status_group, 0x00FFFFFF);
    xEventGroupSetBits(_sta_status_group, status);
}
/**
 * Start Wifi connection
 * if passphrase is set the most secure supported mode will be automatically selected
 * @param ssid const char*          Pointer to the SSID string.
 * @param passphrase const char *   Optional. Passphrase. Valid characters in a passphrase must be between ASCII 32-126 (decimal).
 * @param bssid uint8_t[6]          Optional. BSSID / MAC of AP
 * @param channel                   Optional. Channel of AP
 * @param connect                   Optional. call connect
 * @return
 */
wl_status_t WifiIF::begin(const char *ssid, const char *passphrase, int32_t channel, const uint8_t *bssid, bool connect)
{

    if (!enableSTA(true))
    {
        log_e("STA enable failed!");
        return WL_CONNECT_FAILED;
    }

    if (!ssid || *ssid == 0x00 || strlen(ssid) > 31)
    {
        log_e("SSID too long or missing!");
        return WL_CONNECT_FAILED;
    }

    if (passphrase && strlen(passphrase) > 64)
    {
        log_e("passphrase too long!");
        return WL_CONNECT_FAILED;
    }

    wifi_config_t conf;
    memset(&conf, 0, sizeof(wifi_config_t));
    strcpy(reinterpret_cast<char *>(conf.sta.ssid), ssid);

    if (passphrase)
    {
        if (strlen(passphrase) == 64)
        { // it's not a passphrase, is the PSK
            memcpy(reinterpret_cast<char *>(conf.sta.password), passphrase, 64);
        }
        else
        {
            strcpy(reinterpret_cast<char *>(conf.sta.password), passphrase);
        }
    }

    if (bssid)
    {
        conf.sta.bssid_set = 1;
        memcpy((void *)&conf.sta.bssid[0], (void *)bssid, 6);
    }

    if (channel > 0 && channel <= 13)
    {
        conf.sta.channel = channel;
    }

    wifi_config_t current_conf;
    esp_wifi_get_config(WIFI_IF_STA, &current_conf);
    if (!sta_config_equal(current_conf, conf))
    {
        if (esp_wifi_disconnect())
        {
            log_e("disconnect failed!");
            return WL_CONNECT_FAILED;
        }

        esp_wifi_set_config(WIFI_IF_STA, &conf);
    }
    else if (status() == WL_CONNECTED)
    {
        return WL_CONNECTED;
    }
    else
    {
        esp_wifi_set_config(WIFI_IF_STA, &conf);
    }

    if (!_useStaticIp)
    {
        if (tcpip_adapter_dhcpc_start(TCPIP_ADAPTER_IF_STA) == ESP_ERR_TCPIP_ADAPTER_DHCPC_START_FAILED)
        {
            log_e("dhcp client start failed!");
            return WL_CONNECT_FAILED;
        }
    }
    else
    {
        tcpip_adapter_dhcpc_stop(TCPIP_ADAPTER_IF_STA);
    }

    if (connect && esp_wifi_connect())
    {
        log_e("connect failed!");
        return WL_CONNECT_FAILED;
    }

    return status();
}

wl_status_t WifiIF::begin(char *ssid, char *passphrase, int32_t channel, const uint8_t *bssid, bool connect)
{
    return begin((const char *)ssid, (const char *)passphrase, channel, bssid, connect);
}

/**
 * Use to connect to SDK config.
 * @return wl_status_t
 */
wl_status_t WifiIF::begin()
{
    mLog << "Trying to reconnect ...." <<ILogger::end;
    if (!enableSTA(true))
    {
        log_e("STA enable failed!");
        return WL_CONNECT_FAILED;
    }

    wifi_config_t current_conf;
    if (esp_wifi_get_config(WIFI_IF_STA, &current_conf) != ESP_OK || esp_wifi_set_config(WIFI_IF_STA, &current_conf) != ESP_OK)
    {
        log_e("config failed");
        return WL_CONNECT_FAILED;
    }

    if (!_useStaticIp)
    {
        if (tcpip_adapter_dhcpc_start(TCPIP_ADAPTER_IF_STA) == ESP_ERR_TCPIP_ADAPTER_DHCPC_START_FAILED)
        {
            log_e("dhcp client start failed!");
            return WL_CONNECT_FAILED;
        }
    }
    else
    {
        tcpip_adapter_dhcpc_stop(TCPIP_ADAPTER_IF_STA);
    }

    if (status() != WL_CONNECTED && esp_wifi_connect())
    {
        log_e("connect failed!");
        return WL_CONNECT_FAILED;
    }

    return status();
}

/**
 * will force a disconnect an then start reconnecting to AP
 * @return ok
 */
bool WifiIF::reconnect()
{
    if (getMode() & WIFI_MODE_STA)
    {
        if (esp_wifi_disconnect() == ESP_OK)
        {
            return esp_wifi_connect() == ESP_OK;
        }
    }
    return false;
}

/**
 * Disconnect from the network
 * @param wifioff
 * @return  one value of wl_status_t enum
 */
bool WifiIF::disconnect(bool wifioff, bool eraseap)
{
    wifi_config_t conf;

    if (getMode() & WIFI_MODE_STA)
    {
        if (eraseap)
        {
            memset(&conf, 0, sizeof(wifi_config_t));
            if (esp_wifi_set_config(WIFI_IF_STA, &conf))
            {
                log_e("clear config failed!");
            }
        }
        if (esp_wifi_disconnect())
        {
            log_e("disconnect failed!");
            return false;
        }
        if (wifioff)
        {
            return enableSTA(false);
        }
        return true;
    }

    return false;
}

/**
 * is STA interface connected?
 * @return true if STA is connected to an AD
 */
bool WifiIF::isConnected()
{
    return (status() == WL_CONNECTED);
}

/**
 * Setting the ESP32 station to connect to the AP (which is recorded)
 * automatically or not when powered on. Enable auto-connect by default.
 * @param autoConnect bool
 * @return if saved
 */
bool WifiIF::setAutoConnect(bool autoConnect)
{
    const esp_timer_create_args_t periodic_timer_args = {
        .callback = &timer,
        .arg = this,
        /* name is optional, but may help identify the timer when debugging */
        /*.name = "Sensor_check"*/};
    esp_timer_handle_t periodic_timer;
    ESP_ERROR_CHECK(esp_timer_create(&periodic_timer_args, &periodic_timer));
    ESP_ERROR_CHECK(esp_timer_start_periodic(periodic_timer, 10000000));
    return false; //now deprecated
}

void WifiIF::timer(void *This)
{
    auto obj = static_cast<WifiIF *>(This);
    obj->mLog << "Timer sjdikjaskdj;lsd;ls;ldj;LSJD;LSD;LJklsdjklaSDKLJKLAsjdklaJSDKJkl" << ILogger::end;
    int check = obj->getStatusBits() & STA_CONNECTED_BIT;
    obj->mLog << "Wifi status: " << check << ILogger::end;
    if (check == 0)
    {
        obj->mLog << "Timer check" << ILogger::end;
        //obj->reconnect();
    }

    obj->mLog << "Timer end" << ILogger::end;
}
/**
 * Checks if ESP32 station mode will connect to AP
 * automatically or not when it is powered on.
 * @return auto connect
 */
bool WifiIF::getAutoConnect()
{
    /*bool autoConnect;
    esp_wifi_get_auto_connect(&autoConnect);
    return autoConnect;*/
    return false; //now deprecated
}

bool WifiIF::setAutoReconnect(bool autoReconnect)
{
    _autoReconnect = autoReconnect;
    return true;
}

bool WifiIF::getAutoReconnect()
{
    return _autoReconnect;
}

/**
 * Wait for WiFi connection to reach a result
 * returns the status reached or disconnect if STA is off
 * @return wl_status_t
 */
uint8_t WifiIF::waitForConnectResult()
{
    //1 and 3 have STA enabled
    if ((getMode() & WIFI_MODE_STA) == 0)
    {
        return WL_DISCONNECTED;
    }
    int i = 0;
    while ((!status() || status() >= WL_DISCONNECTED) && i++ < 100)
    {
        delay(100);
    }
    return status();
}

/**
 * Get the station interface IP address.
 * @return IPAddress station IP
 */
IPAddress WifiIF::localIP()
{
    if (getMode() == WIFI_MODE_NULL)
    {
        return IPAddress();
    }
    tcpip_adapter_ip_info_t ip;
    tcpip_adapter_get_ip_info(TCPIP_ADAPTER_IF_STA, &ip);
    return IPAddress(ip.ip.addr);
}
