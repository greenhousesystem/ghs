#include "IPAddress.h"
#include "WiFiLink.h"
#include "WiFiType.h"
#include "Logger.h"

class WifiIF :public WifiLink
{
   public:
    wl_status_t begin(const char* ssid, const char *passphrase = NULL, int32_t channel = 0, const uint8_t* bssid = NULL, bool connect = true);
    wl_status_t begin(char* ssid, char *passphrase = NULL, int32_t channel = 0, const uint8_t* bssid = NULL, bool connect = true);
    wl_status_t begin();

    bool config(IPAddress local_ip, IPAddress gateway, IPAddress subnet, IPAddress dns1 = (uint32_t)0x00000000, IPAddress dns2 = (uint32_t)0x00000000);

    bool reconnect();
    bool disconnect(bool wifioff = false, bool eraseap = false);

    bool isConnected();

    bool setAutoConnect(bool autoConnect);
    bool getAutoConnect();

    bool setAutoReconnect(bool autoReconnect);
    bool getAutoReconnect();

    uint8_t waitForConnectResult();

    // STA network info
    IPAddress localIP();
    
    static wl_status_t status();
   private:
   static void timer(void *thisPtr);
   void _setStatus(wl_status_t status);
   private:
    static bool _useStaticIp;
    static bool _autoReconnect;
    Logger mLog;
};