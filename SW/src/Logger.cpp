#include "Logger.h"
#define LOG_LOCAL_LEVEL ESP_LOG_VERBOSE
#include "esp_log.h"
#include "esp32-hal-log.h"

Logger &Logger::operator<<(const float data)
{
    //log_printf("[%s:%u] %s(): ",pathToFileName(__FILE__), __LINE__, __FUNCTION__);
    log_printf("%f", data);
    return *this;
}

Logger &Logger::operator<<(const int data)
{
    //log_printf("[%s:%u] %s(): ",pathToFileName(__FILE__), __LINE__, __FUNCTION__);
    log_printf("%d", data);
    return *this;
}

Logger &Logger::operator<<(const char *data)
{
    //log_printf("[%s:%u] %s(): ",pathToFileName(__FILE__), __LINE__, __FUNCTION__);
    log_printf("%s", data);
    return *this;
}

Logger &Logger::operator<<(const std::string &data)
{
    //log_printf("[%s:%u] %s(): ",pathToFileName(__FILE__), __LINE__, __FUNCTION__);
    log_printf("%s", data.c_str());
    return *this;
}