#pragma once
#include "ILogger.h"

class Logger : public ILogger
{
public:
    Logger &operator<<(const std::string &data) override;

    Logger &operator<<(const char *data) override;

    Logger &operator<<(const int data) override;

    Logger &operator<<(const float data) override;
};