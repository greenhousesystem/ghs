#include "MotorService.h"
#include "HardwareSerial.h"
MotorService::MotorService(Hal& hal,ILogger& log): mHal(hal),mLog(log)
{
    mThread = std::thread([this]()
    {
        run();
    });
}

void MotorService::run()
{
    while(true)
    {
        int data;
        mHal.hal_read_Depth(&data);
        mLog << "Distance: " << data <<"mm" << ILogger::end;
        std::this_thread::sleep_for(std::chrono::milliseconds(2000));
    }
}
