#pragma once
#include "Hal/Hal.h"
#include <thread>
#include "ILogger.h"

class MotorService
{
private:
    Hal& mHal;
    std::thread mThread;
    ILogger& mLog;
public:
    MotorService(Hal& hal,ILogger& log);
    void run();
};

