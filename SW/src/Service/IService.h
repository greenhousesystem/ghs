#ifndef ISERVICE_H_
#define ISERVICE_H_

#include <string>

/** Interface for async services.
 *
 * This interface defines the common interface for all async services.
 */
class IService
{
public:
    virtual ~IService() = default;

    /** The name of the service.
     */
    virtual const std::string& getName() const = 0;

    /** This function is called by chamberlain upon startup
     *
     * The different implementations of IService interface can change start up behaviour.
     */
    virtual void start() = 0;

    /** This function is called by chamberlain when the system should stop.
     *
     * Different implementations of IService interface can change shutdown behaviour.
     */
    virtual void stop() = 0;
};




#endif