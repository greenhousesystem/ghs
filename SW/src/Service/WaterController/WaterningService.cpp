#include "WaterningService.h"

WaterningService::WaterningService(Hal &hal, ILogger &log) : mHal(hal), mLog(log)
{
    mThread = std::thread([this]() {
        checkHumidty();
    });
}

void WaterningService::checkHumidty()
{
    while (true)
    {
        float data;
        mHal.hal_read_temperature(GHS_TEMP_SENSOR_ZONE1, data);
        mLog << "Temperature in celsius: " << data << ILogger::end;
        std::this_thread::sleep_for(std::chrono::milliseconds(2000));
    }
}
