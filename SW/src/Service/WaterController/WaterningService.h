#pragma once
#include "Hal/Hal.h"
#include <thread>
#include "ILogger.h"

class WaterningService
{
private:
    Hal& mHal;
    std::thread mThread;
    ILogger& mLog;
public:
    WaterningService(Hal& hal,ILogger& log);
    void checkHumidty();
};

