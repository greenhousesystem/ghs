/*
 *  This sketch demonstrates how to scan WiFi networks.
 *  The API is almost the same as with the WiFi Shield library,
 *  the most obvious difference being the different file you need to include:
 */

#include "Logger.h"
#include "hal/Sensor/DistanceSensorFactory.h"
#include "hal/Sensor/TemperatureFactory.h"
#include "Service/WaterController/WaterningService.h"
#include "Service/DepthDetection/MotorService.h"
#include "Hal/Hal.h"
#include "FreeRTOS.h"


static Logger logger;

void setup()
{
    logger << std::string("GHS Initializing") << ILogger::end;                                                                                            // Set WiFi to station mode and disconnect from an AP if it was previously connected
}

void loop()
{
    static int start = 1;
    static Hal hal(logger);
    if (start)
    {
        static iSensor *tempsensor = TemperatureFactory().createTemperatureSensor();
        static iSensor *humiditySensor = DistanceSensorFactory().createHumiditySensor();

        hal.addSensor(tempsensor);
        hal.addSensor(humiditySensor);
        hal.hal_ghs_init();
        

        static WaterningService obj(hal, logger);
        static MotorService fsobj(hal,logger);

        start = 0;
    }
    // Wait a bit before scanning again
    vTaskDelete(NULL);
}