/* Template APIs for HAL */

#include <iostream>
#include <stdio.h>

// Include files for temperature sensor
#include <OneWire.h>
#include <DallasTemperature.h>

#define GHS_BAUD_RATE 115200 //TODO: should move to config file
#define GHS_ZONE1_DATA_LINE 4

typedef enum {
	RESRVOIR_PUMP_ON=1,
	RESRVOIR_PUMP_OFF,
	GHS_PUMP_ON,
	GHS_PUMP_OFF,
	GHS_MIST_ON,
	GHS_MIST_OFF,
	GHS_EXHAUST_ON,
	GHS_EXHAUST_OFF,
	GHS_DRIP_ON,
	GHS_DRIP_OFF,
	GHS_TEMP_HIGH,
	GHS_MIST_HIGH,
	GHS_MOISTURE_HIGH,
	GHS_CONF_PARAM_UPDATE
} hal_ghs_event_t;

//TODO set correct values
typedef enum {
	GHS_MIST=1,
	GHS_TEMP=1,
	GHS_MOISTURE=1
} hal_default_conf_val_t;

typedef struct ghs_config_param {
	int ghs_humidity;
	int ghs_temparature;
	int ghs_moisture;
} ghs_config_param_t;

typedef enum {
	GHS_TEMP_SENSOR_ZONE1=1,
	GHS_TEMP_SENSOR_ZONE2,
	GHS_TEMP_SENSOR_ZONE3,
	GHS_TEMP_SENSOR_ZONE4
} ghs_temp_sensor_zone_t;

typedef enum {
	GHS_HAL_INACTIVE,
	GHS_HAL_ACTIVE,
} ghs_hal_state_t;

static ghs_hal_state_t ghsHalState = GHS_HAL_INACTIVE;

class GhsHalClass
{

public:
	GhsHalClass() = default; 

private:
	static int hal_ghs_init();
	{
		// Start the Serial Monitor
  		Serial.begin(GHS_BAUD_RATE);

		//Temperature sensor configuration
		// GPIO where the DS18B20 is connected to
		const int oneWireBus = GHS_ZONE1_DATA_LINE;     
		// Setup a oneWire instance to communicate with any OneWire devices
		OneWire oneWire(oneWireBus);
		// Pass our oneWire reference to Dallas Temperature sensor 
		DallasTemperature sensors(&oneWire);
		// Start the DS18B20 sensor
		sensors.begin();

		ghsHalState = GHS_HAL_ACTIVE;
		std::cout << "GHS HAL initialized" << std::endl;
	}
	
	static int hal_pump_status(BOOL);
	static int hal_start_pump(void);
	static int hal_stop_pump(void);
	static int hal_mist_status(BOOL);
	static int hal_start_mist(void);
	static int hal_stop_mist(void);
	static int hal_drip_status(BOOL);
	static int hal_start_drip(void);
	static int hal_stop_drip(void);
	static int hal_start_exhaust_fan(void);
	static int hal_stop_exhaust_fan(void);
	static int hal_start_suction_fan(void);
	static int hal_stop_suction_fan(void);
	static int hal_read_temparature(ghs_temp_sensor_zone_t ghs_zone, float *temp);
	{
		sensors.requestTemperatures();
		*temp = sensors.getTempCByIndex(0); //TODO: can take an avg too
	}
	static int hal_read_ph(int *pH);
	static int hal_read_soil_moisture(int *moistVal);
	static int hal_read_humidity(int *humiVal);
	static int hal_calc_water_reqmnt(int disperTime, int *vol); /* return water requirement for disperTime in vol */
	static int hal_water_availability(int volQuantity, int *status); /* Return -1 in status for not enough water */
	static int hal_reservoir_water_availability(int *resVol); /* return water volume in the main reservoir */
	static int hal_start_reservoir_pump(void); /* Start pump to fill main reservoir */
	static int hal_reservoir_pump_status(BOOL);
	static int hal_stop_reservoir_pump(void); /* if automated, could be redundant */
	static int hal_event_log(hal_ghs_event, ctime time, char *description); /* Log event type with description at time */

	/* could move to config src file */
	static int ghs_set_config_params(ghs_config_param config_params); /* Set threshold parameter values */
};
